type cell = [ `Neutral | `Mine | `Theirs ];;

type macro_cell = [ `Neutral | `Mine | `Theirs | `Active ];;

type game_info =
 {
   mutable timebank : int;
   mutable time_per_move : int;
   mutable player_names : string list;
   mutable your_bot : string;
   mutable your_botid : int;
 }
;;

type game_state =
 {
   mutable board : cell array array;
   mutable macroboard : macro_cell array array;
   mutable round : int;
   mutable move : int;
   mutable last_update : float;
   mutable last_timebank : int;
   setup : game_info;
 }
;;


